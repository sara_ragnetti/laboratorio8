package it.unibo.oop.lab.mvc;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class UseController implements Controller {
	private final List<String> stringHistory = new LinkedList<>();
	private String nextString;
	
	@Override
	public void setNextString(String nextString) {
        this.nextString = Objects.requireNonNull(nextString, "This method does not accept null values.");
	}

	@Override
	public String getNextStringToPrint() {
		return this.nextString;
	}

	@Override
	public List<String> getPrintedStringHistory() {
		return this.stringHistory;
	}

	@Override
	public void printCurrentString() {
		if(this.nextString == null) {
			throw new IllegalStateException();
		}
		System.out.println(this.nextString);
		stringHistory.add(this.nextString);
	}

}