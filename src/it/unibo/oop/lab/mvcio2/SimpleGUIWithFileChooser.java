package it.unibo.oop.lab.mvcio2;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;


import it.unibo.oop.lab.mvcio.Controller;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUIWithFileChooser {
    
	private final JFrame frame = new JFrame("My Second graphical interface");

	public SimpleGUIWithFileChooser(Controller c) {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    
	    final JPanel panel = new JPanel();
	    final JButton save = new JButton("Save");
	    final JTextArea text = new JTextArea();

	    panel.setLayout(new BorderLayout());
	    frame.setContentPane(panel);
	    
		panel.add(text);
	    panel.add(save, BorderLayout.SOUTH);
	    
	    save.addActionListener(new ActionListener() {
	       
	    	@Override
	        public void actionPerformed(final ActionEvent e) {
	           try {
	        	   c.saveOnFile(text.getText());
	           } catch (IOException e1) {
	        	   System.out.println("Something went wrong...");
	        	   e1.printStackTrace();
	           }
	        }
	    });
	    /*
	     * Starting from the application in mvcio:
	     * 
	     * 1) Add a JTextField and a button "Browse..." on the upper part of the
	     * graphical interface.
	     * Suggestion: use a second JPanel with a second BorderLayout, put the panel
	     * in the North of the main panel, put the text field in the center of the
	     * new panel and put the button in the line_end of the new panel.OKKKKK
	     * 
	     * 2) The JTextField should be non modifiable. And, should display the
	     * current selected file.
	     * 
	     * 3) On press, the button should open a JFileChooser. The program should
	     * use the method showSaveDialog() to display the file chooser, and if the
	     * result is equal to JFileChooser.APPROVE_OPTION the program should set as
	     * new file in the Controller the file chosen. If CANCEL_OPTION is returned,
	     * then the program should do nothing. Otherwise, a message dialog should be
	     * shown telling the user that an error has occurred (use
	     * JOptionPane.showMessageDialog()).
	     * 
	     * 4) When in the controller a new File is set, also the graphical interface
	     * must reflect such change. Suggestion: do not force the controller to
	     * update the UI: in this example the UI knows when should be updated, so
	     * try to keep things separated.
	     */
	    
	    final JPanel secondPanel = new JPanel();
	    secondPanel.setLayout(new BorderLayout());
	    panel.add(secondPanel, BorderLayout.NORTH);
	    
	    final JTextField textField = new JTextField();
	    secondPanel.add(textField, BorderLayout.CENTER);
	    
	    final JButton Browse = new JButton("Browse...");
	    secondPanel.add(Browse, BorderLayout.LINE_END);
	    Browse.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser("Choose where to save");
				int n = fileChooser.showSaveDialog(secondPanel);
				
				if(n == JFileChooser.APPROVE_OPTION) {
					c.setFile(fileChooser.getSelectedFile());
				} 
				else if (n == JFileChooser.CANCEL_OPTION) {}
				else {
					JOptionPane.showMessageDialog(Browse, "an error has occurred...");	
				}			
			}
	    });   
	    
	    final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 4, sh / 4);
        frame.setLocationByPlatform(true);
	}
	
	 private void display() {
	        frame.setVisible(true);
	    }
	
	public static void main(String[] args) {
        final SimpleGUIWithFileChooser gui = new SimpleGUIWithFileChooser(new Controller());
        gui.display();
    }
}
